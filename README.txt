Improvements made:

Made a two-step (kind of three step) heuristic function:
-Beginning of game, the strategy should be to try to stay clustered in the middle, do not try
to get too many pieces
-Middle of the game to end of the game: the strategy should be focused on getting the 
corners and edges and maximizing your "sum of weights", mobility and center board become
less of a priority
-Very end of game: just try to maximize the number of chips, not weights
By changing the "weight" that each square holds in the middle of the game, I was able to 
better model the strategy of my AI with documented strategies of Othello

Added mobility calculations
-Calculated the net mobility, which is the number of squares you can play in minus the number
of squares the other player can play in: in the beginning of the game, mobility is extremely
important, and so I factored in mobility in the beginning of the game when calculating the
score of a possible board.

Deeper minimax
-Increased minimax to a depth of 5, should allow me to make better decisions if opponent
is deciding in a similar fashion as I am

I also spent a lot of time trying to add a locality sum, where each cell would not just be 
weighted by heuristic but also the configuration of the cells around them (whether that
cell is very vulnerable, in a strategic position, surrounded by a block of same-color cells, etc)
Unfortunately, it became really messy, and by the time I got it completed, it was performing
worse off, so I scrapped it

These improvements can still be made much better - for example, I kind of arbitrarily 
weighted the squares as I logically thought they should be weighted, but these might not
be the perfect weights, or even the right weights altogether. If I had more time, I would 
definitely have tried tried varying the weights, and finding the optimal weight distribution.
Also, I guessed when the right point of the game is to go from beginning game to mid/end
game weighting - this could also be optimized. Finally, there are like one or two opening
situations that result in utter catastrophe, so adding an opening book before going into
the two step heuristic would probably be a good idea.
