#include "board.h"
#include <cstdio>
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board(Side side) {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
    
    heuristic1 = new int[64];
    heuristic2 = new int[64];
    
    //Corners
    heuristic1[0 + 8*0] = 10;
    heuristic1[7 + 8*0] = 10;
    heuristic1[0 + 8*7] = 10;
    heuristic1[7 + 8*7] = 10;
    heuristic2[0 + 8*0] = 10;
    heuristic2[7 + 8*0] = 10;
    heuristic2[0 + 8*7] = 10;
    heuristic2[7 + 8*7] = 10;
   
    //Diagonal Adjacent Corner
    heuristic1[1 + 8*1] = -5;
    heuristic1[6 + 8*1] = -5;
    heuristic1[1 + 8*6] = -5;
    heuristic1[6 + 8*6] = -5;    
    heuristic2[1 + 8*1] = -3;
    heuristic2[6 + 8*1] = -3;
    heuristic2[1 + 8*6] = -3;
    heuristic2[6 + 8*6] = -3;
    
    //Adjacent Corner
    heuristic1[1 + 8*0] = -3;
    heuristic1[6 + 8*0] = -3;
    heuristic1[0 + 8*1] = -3;
    heuristic1[7 + 8*1] = -3;
    heuristic1[0 + 8*6] = -3;
    heuristic1[7 + 8*6] = -3;
    heuristic1[1 + 8*7] = -3;
    heuristic1[6 + 8*7] = -3;
    heuristic2[1 + 8*0] = -2;
    heuristic2[6 + 8*0] = -2;
    heuristic2[0 + 8*1] = -2;
    heuristic2[7 + 8*1] = -2;
    heuristic2[0 + 8*6] = -2;
    heuristic2[7 + 8*6] = -2;
    heuristic2[1 + 8*7] = -2;
    heuristic2[6 + 8*7] = -2;
    
    //Edges
    for(int i = 2; i < 6; i++)
	{
		heuristic1[i + 8*0] = 1;
		heuristic1[i + 8*7] = 1;
		heuristic2[i + 8*0] = 3;
		heuristic2[i + 8*7] = 3;
	}
    for(int j = 2; j < 6; j++)
	{
		heuristic1[0 + 8*j] = 1;
		heuristic1[7 + 8*j] = 1;
		heuristic2[0 + 8*j] = 3;
		heuristic2[7 + 8*j] = 3;
	}
	
	//Center 4x4
	for(int i = 2; i < 6; i++)
		for(int j = 2; j < 6; j++)
			heuristic1[i + 8*j] = 1;
			
	boardSide = side;
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board(boardSide);
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

/*
 * Gets a 64 bit char array that stores the board in such a way that we can
 * pass it back into the setBoard() method to reset the board
 */ 
char* Board::getBoard()
{
	char* board = new char[64];
	for(int i = 0; i < 64; i++)
	{
		if(taken[i] && black[i])
			board[i] = 'b';
		else if(taken[i])
			board[i] = 'w';
		else
			board[i] = ' ';
	}
	return board;
}

/**
 * Calculates the score for a particular board given a side and the
 * heuristic, which weights each square as said in the 64-int heuristic
 * array created earlier
 */ 
int Board::getScore()
{
	int score = 0;
	//End game just try to get more than the other
	if(countBlack() + countWhite() > 56)
		return getBasicScore();
	//Calculating everything for boardSide = black
	for(int i = 0; i < 64; i++)
	{
		if(!taken[i]){}
		else if(black[i])
		{
			if(countBlack() + countWhite() < 20) //beginning game
				score += heuristic1[i];
			else
				score += heuristic2[i];
		}
		else
		{
			if(countBlack() + countWhite() < 20) //beginning game
				score -= heuristic1[i];
			else
				score -= heuristic2[i];
		}
	}
	if(boardSide == WHITE)
		score *= -1;
	//Beginning of game try to maximize your mobility
	if(countBlack() + countWhite() < 24)
		score += 2*getNetMobility();
	return score;
}

/**
 * Wrapper function for other getScore method that makes the nextMove
 * that is passed in, calculates the score of the new board, resets
 * the board, and then returns the score
 * 
int Board::getScore(Move *nextMove, Side side)
{
	char* currBoardState = getBoard();
	doMove(nextMove, side);
	int score = getScore(side);
	setBoard(currBoardState);
	return score;
}
*/

/**
 * Basic heuristic: number of my side - number of other side
 */ 
int Board::getBasicScore()
{
	if(boardSide == BLACK)
		return countBlack() - countWhite();
	else
		return countWhite() - countBlack();
}

Side Board::getBoardSide()
{
	return boardSide;
}

//Gets the net mobility of the boardSide player
int Board::getNetMobility()
{
	int blackMobility = 0;
	int whiteMobility = 0;
	//Count the number of empty squares black and white can play in
	for(int i = 0; i < 64; i++)
	{
		if(!taken[i])
		{
			Move * possMove = new Move((i-i%8)/8, i%8);
			if(checkMove(possMove, BLACK))
				blackMobility++;
			if(checkMove(possMove, WHITE))
				whiteMobility++;
		}
	}
	//Return mobility according to who the player is
	if(boardSide == BLACK)
		return blackMobility - whiteMobility;
	else
		return whiteMobility - blackMobility;
}
