#include "player.h"
#include <stdlib.h>
#include <stdio.h>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    myBoard = new Board(side);
	mySide = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
	//Process opponent's move
    Side otherSide = (mySide == BLACK) ? WHITE : BLACK;
    myBoard->doMove(opponentsMove, otherSide);
    Move * myMove = NULL;
    
    //Any move will have a score better than -1000 with heuristic
    int bestScore = -1000;
    //Go through all squares
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			Move * possMove = new Move(i, j);
			//If can move into that square
			if(myBoard->checkMove(possMove, mySide))
			{
				int possScore = getMinimax(myBoard, 5, mySide);
				
				//update myMove (best move to take overall)	and bestScore
				//found so far (maximizing score)
				if(possScore > bestScore)
				{
					myMove = possMove;
					bestScore = possScore;
				}
			}
		}
	}
	//Make the best move and return it
    myBoard->doMove(myMove, mySide);
	return myMove;
}

//Sets the internal board object to the char array passed in
void Player::setBoard(char boardData[])
{
	myBoard->setBoard(boardData);
}

//Minimax algorithm
int Player::getMinimax(Board * board, int depth, Side side)
{
    if(depth == 0);
        return board->getScore();
    Side oppSide = (side == BLACK) ? WHITE : BLACK;
    Board * copy = board->copy();
    int bestValue = 0;
    bool moveExists = false;
    if(side == board->getBoardSide()) //maximizing side
    {
        bestValue = -10000;
        for(int i = 0; i < 64; i++)
		{
			Move * possMove = new Move((i-i%8)/8, i%8);
			if(copy->checkMove(possMove, side))
			{
				moveExists = true;
				copy->doMove(possMove, side);
				int value = getMinimax(copy, depth - 1, oppSide);
				if(value > bestValue)
					bestValue = value;
			}
		}
    }
    else //minimizing side
    {
        bestValue = 10000;
        for(int i = 0; i < 64; i++)
		{
			Move * possMove = new Move((i-i%8)/8, i%8);
			if(copy->checkMove(possMove, side))
			{
				moveExists = true;
				copy->doMove(possMove, side);
				int value = getMinimax(copy, depth - 1, oppSide);
				if(value < bestValue)
					bestValue = value;
			}
        }
     }
     //Make sure something happened, otherwise return score of this
     //board (depth variable was greater than actual depth of branch)
     if(moveExists)
		return bestValue;
	 else
		return myBoard->getScore();
}
